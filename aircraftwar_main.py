import os
os.environ["PYGAME_HIDE_SUPPORT_PROMPT"] = ""
from aircraftwar_sprites import *


class AircraftWar(object):
    """Aircraft War main game"""

    def __init__(self):
        print("game initial...")
        pygame.init()
        # Create a window for the game
        self.screen = pygame.display.set_mode(SCREEN_RECT.size)
        # Modify the form display name
        pygame.display.set_caption('Aircraft War')
        # loading background music
        pygame.mixer.init()
        pygame.mixer.music.load("./music/Allure.mp3")
        # the song starts at 33 seconds， -1 : loop play
        pygame.mixer.music.play(-1, 33)

        # Create the game clock
        self.clock = pygame.time.Clock()
        # Calling private methods，Create sprites and Sprite groups
        self.__create_sprites()
        # User score
        AircraftWar.score = 0

        # Set the timer event
        pygame.time.set_timer(CREATE_ENEMY_EVENT, 1000)  # Create the enemy, 1 s
        pygame.time.set_timer(HERO_FIRE_EVENT, 500)   # Create bullet,500ms

    def __create_sprites(self):
        # Create background sprites and Sprite groups
        bg1 = Background()
        bg2 = Background(True)
        self.back_group = pygame.sprite.Group(bg1, bg2)
        # Create enemy Sprite group
        self.enemy_group = pygame.sprite.Group()
        # Create the destroy Sprite group
        self.destroy_group = pygame.sprite.Group()
        # Create the hero's sprite and sprite group
        self.hero = Hero()
        self.hero_group = pygame.sprite.Group(self.hero)

    def start_game(self):
        """start game"""
        print("game start...")

        while True:
            # 1. set the refresh frame rate
            self.clock.tick(FRAME_PER_SEC)

            # 2. event listening
            self.__event_handler()

            # 3. collision detection
            self.__check_collide()

            # 4. update sprite group
            self.__update_sprites()

            # 5. update screen display
            pygame.display.update()

    def __event_handler(self):
        """event listening"""
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                AircraftWar.__game_over()
            elif event.type == CREATE_ENEMY_EVENT:
                # create enemy plane sprites
                enemy = Enemy()
                # add enemy Sprite to this group
                self.enemy_group.add(enemy)
            elif event.type == HERO_FIRE_EVENT:
                self.hero.fire()

            # Determine if the hero has been destroyed，if true，game over！
            if self.hero.can_destroy:
                AircraftWar.__game_over()

            # Get the keyboard keys using the method provided by the keyboard - The key tuples
            keys_pressed = pygame.key.get_pressed()
            # Determines the corresponding key index value in the tuple
            if keys_pressed[pygame.K_RIGHT]:
                self.hero.speed = 2
            elif keys_pressed[pygame.K_LEFT]:
                self.hero.speed = -2
            else:
                self.hero.speed = 0

    def __check_collide(self):
        """collision detection"""
        # bullets destroyed the enemy plane
        enemies = pygame.sprite.groupcollide(self.enemy_group,
                                             self.hero.bullets,
                                             False,
                                             True).keys()
        for enemy in enemies:
            enemy.life -= 1

            if enemy.life <= 0:
                enemy.add(self.destroy_group)
                enemy.remove(self.enemy_group)

                enemy.destroy()
                AircraftWar.score += 1

        # the enemy plane crashed into the hero
        for hero in pygame.sprite.spritecollide(self.hero,
                                                self.enemy_group,
                                                True):
            self.hero.destroy()

    def __update_sprites(self):
        """update sprite group"""
        # self.back_group.update()
        # self.back_group.draw(self.screen)
        #
        # self.enemy_group.update()
        # self.enemy_group.draw(self.screen)
        #
        # self.hero_group.update()
        # self.hero_group.draw(self.screen)
        #
        # self.hero.bullets.update()
        # self.hero.bullets.draw(self.screen)
        #
        # self.destroy_group.update()
        # self.destroy_group.draw(self.screen)

        # code optimization，update sprite groups by looping through them
        for group in [self.back_group, self.hero_group,
                      self.hero.bullets, self.enemy_group,
                      self.destroy_group]:
            group.update()
            group.draw(self.screen)

    @classmethod
    def __game_over(cls):
        """game over"""
        print("game over...")
        print(f"Your Score is {cls.score}")
        pygame.quit()
        exit()


if __name__ == '__main__':
    # create game objects
    game = AircraftWar()

    # start game
    game.start_game()
