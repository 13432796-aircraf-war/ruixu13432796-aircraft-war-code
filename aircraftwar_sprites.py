import random
import pygame

# Screen size constant
SCREEN_RECT = pygame.Rect(0, 0, 480, 700)
# Screen refresh frame rate
FRAME_PER_SEC = 60
# Creates a timer constant for enemy aircraft
CREATE_ENEMY_EVENT = pygame.USEREVENT
# Hero fires bullet event Timer constant
HERO_FIRE_EVENT = pygame.USEREVENT + 1


class GameSprite(pygame.sprite.Sprite):
    """
    Aircraft war game Sprite class
    image_name: The name of the image to display
    speed: flight speed
    """
    def __init__(self, image_name, speed=1):
        # Because the inherited parent is not an Object class，So make sure you start with super().__init__()
        super().__init__()
        # load image
        self.image = pygame.image.load(image_name)
        # Set the size
        self.rect = self.image.get_rect()
        # recording speed（The default value of 1）
        self.speed = speed

    def update(self, *args):
        # Move in the vertical direction by default(Simulated dynamic effect)
        self.rect.y += self.speed

    @staticmethod
    def image_names(prefix, count):
        # Get all the images，add to list
        names = []
        for i in range(1, count + 1):
            names.append("./images/" + prefix + str(i) + ".png")

        return names


class Background(GameSprite):
    """Background  Sprite"""
    def __init__(self, is_alt=False):
        super().__init__("./images/background.png")

        # If is_alt is True it represents the second background image，
        # The initial status is displayed directly above the screen
        if is_alt:
            self.rect.bottom = 0

    def update(self, *args):
        super().update(args)

        if self.rect.top >= SCREEN_RECT.height:
            self.rect.bottom = 0


class PlaneSprite(GameSprite):
    """Plane Sprite，Including enemy and heroes"""
    def __init__(self, image_names, destroy_names, life, speed):

        image_name = image_names[0]
        super().__init__(image_name, speed)

        # life
        self.life = life

        # List of normal images
        self.__life_images = []
        for file_name in image_names:
            image = pygame.image.load(file_name)
            self.__life_images.append(image)

        # List of destroyed images
        self.__destroy_images = []
        for file_name in destroy_names:
            image = pygame.image.load(file_name)
            self.__destroy_images.append(image)

        # By default, live images are played
        self.images = self.__life_images
        # Display image index
        self.show_image_index = 0
        # isLooping
        self.is_loop_show = True
        # Can it be deleted
        self.can_destroy = False

    def update(self, *args):
        self.update_images()

        super().update(args)

    def update_images(self):
        """Update the image"""
        pre_index = int(self.show_image_index)
        self.show_image_index += 0.05
        count = len(self.images)

        # Determine whether to loop
        if self.is_loop_show:
            self.show_image_index %= len(self.images)
        elif self.show_image_index > count - 1:
            self.show_image_index = count - 1
            self.can_destroy = True

        current_index = int(self.show_image_index)

        if pre_index != current_index:
            self.image = self.images[current_index]

    def destroy(self):
        """plane was destroyed"""

        # By default, live images are played
        self.images = self.__destroy_images
        # Display image index
        self.show_image_index = 0
        # isLooping
        self.is_loop_show = False


class Enemy(PlaneSprite):
    """Enemy Sprite"""
    def __init__(self):
        image_names = ["./images/enemy1.png"]
        destroy_names = GameSprite.image_names("enemy1_down", 4)

        # Call the superclass method,Create enemy plane sprites, specify the image of the enemy aircraft
        super().__init__(image_names, destroy_names, 2, 1)
        # Set random speed for enemy aircraft 1-3
        self.speed = random.randint(1, 3)
        # Sets the random initial position of enemy aircraft
        # The rightmost value of the horizontal position of the enemy aircraft
        width = SCREEN_RECT.width - self.rect.width
        self.rect.left = random.randint(0, width)
        self.rect.bottom = 0

    def update(self, *args):
        # Call the superclass method，Let the enemy planes move in a vertical direction
        super().update(args)

        # Determine whether to fly off the screen，if true，Enemy planes need to be removed from the Sprite group
        if self.rect.y >= SCREEN_RECT.height:
            # Remove sprites from all groups
            self.kill()

        # Determine if the enemy aircraft has been destroyed
        if self.can_destroy:
            self.kill()


class Hero(PlaneSprite):
    """hero sprite"""

    def __init__(self):
        image_names = GameSprite.image_names("me", 2)
        destroy_names = GameSprite.image_names("me_destroy_", 4)

        super().__init__(image_names, destroy_names, 0, 0)
        # Set Initial Position
        self.rect.centerx = SCREEN_RECT.centerx
        self.rect.bottom = SCREEN_RECT.bottom - 80
        # Create bullet_group
        self.bullets = pygame.sprite.Group()

    def update(self, *args):
        self.update_images()

        # Horizontal movement of aircraft
        self.rect.left += self.speed

        # Out of screen detection
        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.right > SCREEN_RECT.right:
            self.rect.right = SCREEN_RECT.right

    def fire(self):
        # Fire 3 in a row at a time
        for i in range(0, 3):
            # Create bullet sprites
            bullet = Bullet()

            # Set bullet position
            bullet.rect.bottom = self.rect.top - i * 20
            bullet.rect.centerx = self.rect.centerx

            # Adds bullets to this Sprite group
            self.bullets.add(bullet)


class Bullet(GameSprite):
    """bullet sprites"""
    def __init__(self):
        image_name = "./images/bullet1.png"
        super().__init__(image_name, -2)

    def update(self):
        super().update()

        # Determine if it is off screen，if true，delete from Sprite group
        if self.rect.bottom < 0:
            self.kill()
